
const fs = require('fs')
const express = require('express')
const app = express();
const port = 3000;
const path = require('path')
const uuid = require('uuid')
var helmet = require('helmet');
// var bodyParser = require('body-parser');

app.use(function (req,res,next) {
    console.log('Request : ',req.path);
    next();
})
app.set('view engine','pug')
app.use(helmet());

app.use(express.static(path.join(__dirname, 'public')));

//Executé à chaque requête vers le serveur
// app.use(bodyParser.urlencoded({ extended: false }))
// // parse application/json
// app.use(bodyParser.json())

app.use(express.json())
app.use(express.urlencoded({extended:true}))

const filepath = "./citie.json";

app.get('/cities', function (req,res,next) {
    try {
        
        fs.readFile(filepath, (err,data) => {
            if (err) {
                console.log(err)
                next(err)
            }
            else {
                data = JSON.parse(data)
                console.log(data.cities);

                //Rendu du contenu récupéré
                res.render('getcities',{
                    title : "GET cities",
                    data : data.cities
                })
            }            
        });
        
    }
    catch (err) {
        console.log(err);
        next(err)
    }
})

app.post('/city', function (req,res, next) {
    let doublon = false;
    
    try {
        
        fs.readFile(filepath, (err,data) => {
            if (err) {
                console.log(err)
                next(err)
            }
            else {
                const ville = req.body.name
                console.log("ville à ajouter: ",ville)

                data = JSON.parse(data);
                
                doublon = findName(data,ville)

                if (!doublon) {
                    
                    const newville = {"id" : uuid.v4(), "name" : ville};
                    data.cities.push(newville);
                                    
                    fs.writeFile(filepath, JSON.stringify(data), (err) => {
                        if (err) {
                            console.log(err)
                        }
                    } ) 
                }
                else {
                    console.log("La ville existe déjà")
                }

                console.log(doublon)
                let added = (!doublon) ? "La ville "+req.body.name +" a été ajouté" : "La ville "+ req.body.name + " existe déjà"
                console.log(added)
                res.render('updatecity',{
                    title : "POST city",
                    ville: added
                })

            }
        });

        
    }
    catch (err) {
        console.log(err);
        next(err);
        
    }

})
app.put('/city', function (req,res, next) {

    let doublon = false;
    try {
        if (fs.existsSync(filepath)) {
            fs.readFile(filepath, (err,data) => {
                if (err) {
                    console.log(err)
                    next(err)
                }
                else {
                    const ville = req.body.name
                    const id_ville = req.body.id
                    console.log("ville à modifier: ",ville)
    
                    data = JSON.parse(data);
                    objville = findId(data,id_ville)
                    doublon = findName(data,ville)
    
                    if (objville.continu && !doublon) {
                        
                        
                        for (i in data.cities) {
                            console.log(data.cities[i])
                            if (data.cities[i].id === id_ville) {
                                
                              data.cities[i].name = ville;
                              break;
                            }
                          }
                        
                                        
                        fs.writeFile(filepath, JSON.stringify(data), (err) => {
                            if (err) {
                                console.log(err)
                            }
                        } ) 
                    }
                    else {
                        console.log("La ville existe déjà");
                        
                    }
                    const added = (objville.continu == true && !doublon) ? "La ville "+objville.lastname +" a été mise à jour en "+req.body.name : doublon == true ? req.body.name+" existe déjà":"Aucun id trouvé pour "+ req.body.id
                    res.render('updatecity',{
                        title : "PUT city",
                        ville : added
                        
                    })
                }
            });
               
            
        }
    }
    catch (err) {
        console.log(err);
        next(err)
        
    }

})
app.delete('/city/:id', function(req,res,next) {

    try {
        fs.readFile(filepath, (err,data) => {
            if (err) {
                console.log(err)
                next(err)
            }
            else {
                const id_ville = req.params.id;
                data = JSON.parse(data);
                objville = findId(data,id_ville)

                if (objville.continu) {
                    console.log("ville à supprimer: ",objville.lastname)
                    
                    for (i in data.cities) {
                        if (data.cities[i].id === id_ville) {
                            data.cities.splice(i,1)
                            break;
                        }
                    }
             
                    fs.writeFile(filepath, JSON.stringify(data), (err) => {
                        if (err) {
                            console.log(err)
                            next(err);
                        }
                    }) 
                }
                else {
                    console.log("La ville n'existe pas");
                    
                }
                const added = objville.continu == true ? "La ville "+objville.lastname +" a été supprimée " : "Aucune ville trouvée pour l'id "+ req.params.id
                
                res.render('updatecity',{
                    title : "DELETE city",
                    ville : added
                    
                })
            }

        });
        
    }
    
    catch (err) {
        console.log(err);
        next(err)
    }

})
app.use(function (err,req,res,next) {
    console.error(err.stack);
    // res.status(500).send("Something went wrong")
    res.status(500)
    res.render('error', {
        title: "ERROR",
        info: "Server error"
    })

    next();
})

app.listen(port, () => {
    console.log(`API listening on port ${port}`);
});


function findId(data,id_ville) {

    //Parcourt les villes jusqu'à trouver le meme id et retourne le nom de la ville et true
    for (elt in data.cities) {
        town = data.cities[elt];
        if (town.id == id_ville) {
            lastname = town.name;
            return {lastname: lastname,continu:true};
        }
    }
    return {lastname:"",continu:false}
    
}

function findName(data,ville) {
    //Parcourt les villes jusqu'à trouver le meme nom de ville s'il existe
    for (elt in data.cities) {
        town = data.cities[elt];
        if (town.name == ville) {
            return true;
        }
        
    }
    return false;

}